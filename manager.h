#ifndef MANAGER_H
#define MANAGER_H

#include <QDebug>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QSharedPointer>

#include "logger.h"
#include "logexception.h"

#define LOG(LEVEL, MODULE) if(Log::Manager::instance()->needLog(LEVEL, MODULE)) *(Log::Manager::instance()->getLogger()->getNewMessage(Log::Manager::instance()->getLevelName(LEVEL), MODULE))

#define P1(VAR) QPair<QString, QVariant>(#VAR, VAR)
#define P2(NAME, VAR) QPair<QString, QVariant>(NAME, VAR)

namespace Log
{

class Manager
{
public:
    static Manager* instance();

    static QMap<int, QString> createLogLevels(const QStringList &list);

    void setLogLevels(const QMap<int, QString>& levels);
    void setLogLevels(const QStringList& levels);
    void setLogLevel(const int logLevel);
    void setModules(const QStringList& modules);
    void setLogger(Logger* logger);
    void setLogger(QSharedPointer<Logger> logger);

    inline QSharedPointer<Logger>& getLogger()
    { return _logger; }

    inline const QString getLevelName(int level)
    { return _logLevels.value(level); }

    inline bool needLog(int level, QString module)
    {
        if(_logLevels.contains(level) == false){
            throw LogException(QString("Not valid log level given: %1").arg(level));
        }

        if(_modules.contains(module) == false){
            throw LogException(QString("Not valid modul given: %1").arg(module));
        }

        return (_logLevel >= level);
    }

protected:
    Manager();
    Manager(const Manager&);
    Manager& operator =(const Manager&);

private:
    QMap<int, QString> _logLevels;
    int _logLevel;

    QStringList _modules;

    QSharedPointer<Logger> _logger;
};

}

#endif // MANAGER_H
