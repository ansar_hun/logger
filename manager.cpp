#include "manager.h"

using namespace Log;

Manager::Manager()
{
}

Manager::Manager(const Manager &)
{
}

Manager& Manager::operator =(const Manager &)
{
    return *this;
}

Manager *Manager::instance()
{
    static Manager _instance;

    return &_instance;
}

QMap<int, QString> Manager::createLogLevels(const QStringList& list)
{
    QMap<int, QString> logLevels;

    for(int i = 0; i < list.size(); ++i){
        logLevels.insert(i, list.at(i));
    }

    return logLevels;
}

void Manager::setLogLevels(const QMap<int, QString> &levels)
{
    _logLevels = levels;
}

void Manager::setLogLevels(const QStringList &levels)
{
    setLogLevels(createLogLevels(levels));
}

void Manager::setLogLevel(const int logLevel)
{
    _logLevel = logLevel;
}

void Manager::setModules(const QStringList &modules)
{
    _modules = modules;
}

void Manager::setLogger(Logger *logger)
{
    setLogger(QSharedPointer<Logger>(logger));
}

void Manager::setLogger(QSharedPointer<Logger> logger)
{
    _logger = logger;
}
