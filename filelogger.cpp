#include <QTextStream>
#include <QFile>

#include "filelogger.h"

#include "logexception.h"

using namespace Log;

FileLogger::FileLogger(QString file_name):
    Logger()
{
    QFile* file = new QFile(file_name);

    if(file->open(QFile::WriteOnly) == false){
        delete file;
        throw LogException(QString("Can't open the file for write: %1").arg(file_name));
    }

    _stream.setDevice(file);
}

FileLogger::~FileLogger()
{
    _stream.device()->close();
}

void FileLogger::log(const QString &message)
{
    _stream << message << endl;
}
