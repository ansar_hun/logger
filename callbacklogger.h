#ifndef CALLBACKLOGGER_H
#define CALLBACKLOGGER_H

#include "logger.h"

typedef void (*LogHandler)(const QString &);

namespace Log
{

class CallbackLogger : public Logger
{
public:
  CallbackLogger(LogHandler handler);

  virtual void log(const QString &message);

private:
  LogHandler _handler;
};

}

#endif // CALLBACKLOGGER_H
