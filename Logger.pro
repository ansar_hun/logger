#-------------------------------------------------
#
# Project created by QtCreator 2012-01-22T11:16:04
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Logger

CONFIG   += staticlib
TEMPLATE = lib


SOURCES += \
    manager.cpp \
    logger.cpp \
    consolelogger.cpp \
    formatter.cpp \
    filelogger.cpp \
    logexception.cpp \
    callbacklogger.cpp

HEADERS += \
    manager.h \
    logger.h \
    consolelogger.h \
    formatter.h \
    filelogger.h \
    logexception.h \
    callbacklogger.h

QMAKE_CXXFLAGS += -fPIC
