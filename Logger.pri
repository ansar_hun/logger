SOURCES += \
    $$PWD/manager.cpp \
    $$PWD/logger.cpp \
    $$PWD/consolelogger.cpp \
    $$PWD/formatter.cpp \
    $$PWD/filelogger.cpp \
    $$PWD/logexception.cpp

HEADERS += \
    $$PWD/manager.h \
    $$PWD/logger.h \
    $$PWD/consolelogger.h \
    $$PWD/formatter.h \
    $$PWD/filelogger.h \
    $$PWD/logexception.h
