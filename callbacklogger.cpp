#include "callbacklogger.h"

using namespace Log;

CallbackLogger::CallbackLogger(LogHandler handler)
  : Logger(),
    _handler(handler)
{
}

void CallbackLogger::log(const QString &message)
{
  if (_handler)
    {
      _handler(message);
    }
}
