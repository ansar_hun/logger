#ifndef FORMATTER_H
#define FORMATTER_H

#include <QString>
#include <QPair>
#include <QVariant>

namespace Log
{

class Logger;

class Formatter
{
public:
    Formatter(Logger* logger, const QString& level, const QString& module);

    virtual ~Formatter();

    template<class T>
    Formatter& operator<<(const T& value)
    {
        _message += QString(" %1").arg(value);

        return *this;
    }

    Formatter& operator<<(const QPair<QString, QVariant>& value)
    {
        ((((_message += ", ") += value.first) += "='") += value.second.toString()) += "'";

        return *this;
    }

private:
    Logger* _logger;
    QString _message;
};

}

#endif // FORMATTER_H
