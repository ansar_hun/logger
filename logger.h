#ifndef LOGGER_H
#define LOGGER_H

#include <QSharedPointer>

#include "formatter.h"

namespace Log
{

class Logger
{
public:
    Logger();
    virtual ~Logger();

    inline QSharedPointer<Formatter> getNewMessage(const QString& level, const QString& module)
    { return QSharedPointer<Formatter>(new Formatter(this, level, module)); }

protected:
    virtual void log(const QString& message) = 0;

    friend class Formatter;
};

}

#endif // LOGGER_H
