#include <QTextStream>

#include "consolelogger.h"

using namespace Log;

ConsoleLogger::ConsoleLogger():
    Logger(),
    _stream(stdout)
{
}

ConsoleLogger::~ConsoleLogger()
{
}

void ConsoleLogger::log(const QString &message)
{
    _stream << message << endl;
}
