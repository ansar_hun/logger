#ifndef LOGEXCEPTION_H
#define LOGEXCEPTION_H

#include <qtconcurrentexception.h>
#include <QString>

class LogException : public QtConcurrent::Exception
{
public:
    LogException(QString message);
    virtual ~LogException() throw();

    LogException* clone() const;
    void raise() const;

    const QString& getMessage() const;

private:
    QString _message;
};

#endif // LOGEXCEPTION_H
