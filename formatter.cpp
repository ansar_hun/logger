#include "formatter.h"

#include "logger.h"

#include <QDateTime>

using namespace Log;

Formatter::Formatter(Logger *logger, const QString &level, const QString &module):
    _logger(logger)
{
    (((((_message += QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz")) += " (") += module) += ") ") += level) += ":";
}

Formatter::~Formatter()
{
  if (_logger)
    {
      _logger->log(_message);
    }
}
