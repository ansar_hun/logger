#ifndef FILELOGGER_H
#define FILELOGGER_H

#include <QString>

#include "logger.h"

class QTextStream;

namespace Log
{

class FileLogger : public Logger
{
public:
    FileLogger(QString file_name);
    virtual ~FileLogger();

    virtual void log(const QString &message);

private:
    QTextStream _stream;
};

}

#endif // FILELOGGER_H
