#ifndef CONSOLELOGGER_H
#define CONSOLELOGGER_H

#include "logger.h"

class QTextStream;

namespace Log
{

class ConsoleLogger : public Logger
{
public:
    ConsoleLogger();
    virtual ~ConsoleLogger();

    virtual void log(const QString &message);

private:
    QTextStream _stream;
};

}

#endif // CONSOLELOGGER_H
