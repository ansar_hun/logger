#include "logexception.h"

LogException::LogException(QString message):
    QtConcurrent::Exception(),
    _message(message)
{
}

LogException::~LogException() throw()
{
}

LogException *LogException::clone() const
{
    return new LogException(_message);
}

void LogException::raise() const
{
    throw *this;
}

const QString &LogException::getMessage() const
{
    return _message;
}
